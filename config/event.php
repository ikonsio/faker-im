<?php

return [

    # Register your events
    'UserWasRegistered' => [
        'ThemeXpert\Listeners\SendWelcomeEmail',
    ]
];
