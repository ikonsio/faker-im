@extends('layouts.default')

@section('content')
    <div class="container">
        <div class="clearfix main-content">
            <div class="col-md-6 wp-img-dis">
                <h2 class="text-center title">Worry free image <br/>Distribution for WordPress</h2>
                <p>Replacing premium images from theme is time consuming and pain, isn't it?</p>
                <p>Faker.im replace all images url with a nice placeholder on the fly and generate XML file for you. Don’t worry it will take care your image size too.</p>
                <p>Export WordPress XML from your theme demo server and upload it here.</p>

                <form action="{{ url('/') }}" method="post" enctype="multipart/form-data" id="xml-submit-form">
                <div class="input-group">
                    <input type="text" class="form-control" readonly placeholder="No File Chossen">
                    <span class="input-group-btn">
                        <span class="btn btn-primary btn-file">
                            Upload <input type="file" name="xml" required="required">
                        </span>
                    </span>
                </div><!--/.input-group-->
              </form>
            </div><!--/.wp-img-dis-->
            
            <div class="col-md-6 img-placehold">
                <h2 class="text-center title">A Quick & simple <br/>placeholder service.</h2>
                <h3 class="text-center sub-title">Input image size or color code or both after url  and you will get a nice placeholder.</h3>

                <div class="img-tab">
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active"><a href="#img_size" aria-controls="img_size" role="tab" data-toggle="tab">Size</a></li>
                        <li role="presentation"><a href="#img_color" aria-controls="img_color" role="tab" data-toggle="tab">Color</a></li>
                        <li role="presentation"><a href="#img_text" aria-controls="img_text" role="tab" data-toggle="tab">Text</a></li>
                    </ul><!--/.nav-tabs-->

                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane fade in active" id="img_size">
                            <p><span class="label label-success">Width x Height</span><br/> Height is optional, if no height is specified the image will be square.</p>

                            <div class="example">
                                Example : <a href="http://faker.im/300x100" target="_blank" class="bg-ex">http://faker.im/300x100</a>
                            </div><!--/.example-->
                        </div><!--/.tab-pane-->

                        <div role="tabpanel" class="tab-pane fade" id="img_color">
                            <p><span class="label label-success">Background Color / Text Color</span><br/> Provide hex codes(#fff) for color. The first color is always background color second color is text color.</p>

                            <div class="example">
                                Example : <a href="http://faker.im/300x100/999999" target="_blank" class="bg-ex">http://faker.im/300x100/999999</a>
                            </div><!--/.example-->
                        </div><!--/.tab-pane-->

                        <div role="tabpanel" class="tab-pane fade" id="img_text">
                            <p><span class="label label-success">?text=Hello+Faker</span><br/>Custom text can be entered using a query string at the very end of the url. This is optional, default is the image dimensions (300×250). A-z (upper and lowercase), numbers, and most symbols will work just fine. <br/>
                            </p>
                            <p>Combination with size and color is possible. Use + for space.</p>

                            <div class="example">
                                Example : <a href="http://faker.im/?text=Hello+Faker" target="_blank" class="bg-ex">http://faker.im/?text=Hello+Faker</a>
                            </div><!--/.example-->
                        </div><!--/.tab-pane-->
                    </div><!--/.tab-content-->
                </div><!--/.img-tab-->
            </div><!--/.img-placehold-->
        </div>
    </div><!--/.container-->
@stop
