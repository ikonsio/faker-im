<?php

/**
 * For uploading image.
 */
$app->get('/uploader', 'HomeController@showImageUploader');
$app->post('/uploader', 'HomeController@uploadImage');

/**
 * For downloading fake image zip.
 */
$app->get('/uploader/download/{id}', 'HomeController@downloadFakeImageZip');


/**
 * For creating image upload directory.
 */
$app->post('/create-image-upload-directory', 'HomeController@createImageUploadDirectory');


/**
 * For generating placeholder image.
 */
$app->get('/[{size}[/{background}[/{textColor}]]]', 'HomeController@getIndex');
$app->post('/', 'HomeController@postIndex');



