<?php

namespace ThemeXpert\Faker;

use Intervention\Image\ImageManage;
use ThemeXpert\Faker\Contracts\XML as XMLContract;

class XML implements XMLContract
{
    /**
     * Get fake XML file for the faker.im.
     *
     * @param $xml
     *
     * @return mixed|void
     */
    public function getFakeXML($xml)
    {
        $xml = $this->changeGuid($xml);

        $xml = $this->changeWpAttachmentUrl($xml);

        return $xml;
    }

    /**
     * Change guid image link.
     *
     * @param $xml
     *
     * @return mixed
     */
    protected function changeGuid($xml)
    {
        $pattern = '#<guid([^>]+)>http://([^ ]+)/(\S+)(.jpg|png|gif|svg)</guid>#';
        $replace = '<guid ${1}>http://faker.im/?src=http://${2}/${3}${4}</guid>';

        $xml = preg_replace($pattern, $replace, $xml);

        return $xml;
    }

    /**
     * Change wp attachment url link for the image attachment.
     *
     * @param $xml
     *
     * @return mixed
     */
    protected function changeWpAttachmentUrl($xml)
    {
        $pattern = '#<wp:attachment_url><!\[CDATA\[http://([^ ]+)/(\S+)(.jpg|png|gif|svg)\]\]></wp:attachment_url>#';
        $replace = '<wp:attachment_url><![CDATA[http://faker.im/?src=http://${1}/${2}${3}]]></wp:attachment_url>';

        $xml = preg_replace($pattern, $replace, $xml);

        return $xml;
    }

    /**
     * Download fake XML file.
     *
     * @param $originalXML
     * @param $fileName
     *
     * @return mixed
     */
    public function downloadFakeXML($originalXML, $fileName)
    {
        $xmlName = $fileName;

        $xmlPath = storage_path() . "/app/{$xmlName}.xml";

        $fakeXML = $this->getFakeXML($originalXML);

        file_put_contents($xmlPath, $fakeXML);

        return response()->download($xmlPath, true);
    }
}