<?php

namespace ThemeXpert\Faker\Contracts;

interface Image
{
    /**
     * Response fake image.
     *
     * @param null $size
     * @param null $background
     * @param null $textColor
     * @param      $text
     * @param null $imageSource
     *
     * @return string
     */
    public function responseWithFakeImage($size = null, $background = null, $textColor = null, $text = null, $imageSource = null);

    /**
     * Generate fake image zip from the given original image.
     *
     * @param array $images
     *
     * @return int
     */
    public function generateFakeImageZip($images);

    /**
     * Check existence of fake image zip file.
     *
     * @param $id
     *
     * @return bool
     * @throws Exception
     */
    public function fakeImageZipFileExists($id);

    /**
     * Get image upload directory name.
     *
     * @return string
     */
    public function getImageUploadDirName();
}