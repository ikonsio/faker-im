<?php

namespace ThemeXpert\Faker;

use RuntimeException;
use Sun\Routing\UrlGenerator;
use Intervention\Image\ImageManagerStatic as InterventionImage;

class Util
{
    /**
     * Url generator instance.
     *
     * @var \Sun\Routing\UrlGenerator
     */
    protected $urlGenerator;

    /**
     * Google analytics id.
     *
     * @var string
     */
    protected $googleAnalyticsID = 'UA-3036714-19';

    /**
     * Create a new instance of Util.
     *
     * @param UrlGenerator $urlGenerator
     */
    public function __construct(UrlGenerator $urlGenerator)
    {
        $this->urlGenerator = $urlGenerator;
    }

    /**
     * Set color combination for the background and text color.
     */
    public static function getColor()
    {
        return [
            ['background' => '2196F3', 'text' => 'ffffff'],
            ['background' => '673AB7', 'text' => 'ffffff'],
            ['background' => '81C784', 'text' => 'ffffff'],
            ['background' => 'f44336', 'text' => 'ffffff'],
            ['background' => '9C27B0', 'text' => 'ffffff'],
            ['background' => '673AB7', 'text' => 'ffffff'],
            ['background' => '2196F3', 'text' => 'ffffff'],
            ['background' => '8BC34A', 'text' => 'ffffff'],
            ['background' => 'FF9800', 'text' => 'ffffff'],
            ['background' => 'FF5722', 'text' => 'ffffff'],
            ['background' => '1535e9', 'text' => 'ffffff'],
            ['background' => '15c9e9', 'text' => 'ffffff'],
            ['background' => 'ba15e9', 'text' => 'ffffff'],
            ['background' => '4b0547', 'text' => 'ffffff'],
            ['background' => '13946e', 'text' => 'ffffff'],
        ];
    }

    /**
     * Get image size.
     *
     * @param $size
     *
     * @return array
     */
    public static function getImageSize($size)
    {
        if(strpos($size, 'x')) {
            list($width, $height) = explode('x', $size);

            self::validateImageSize($width, $height);

            $size = [
                'width'     => $width,
                'height'    => $height
            ];
        } else {

            if(is_null($size)) {
                $size = 250;
            }

            self::validateImageSize($size, $size);

            $size = [
                'width'     => $size,
                'height'    => $size
            ];
        }

        return $size;
    }

    /**
     * Get placehold.it image response.
     *
     * @param $image
     * @param $background
     * @param $textColor
     * @param $color
     *
     * @return array
     */
    public static function getPlaceholdImageResponse($image, $background, $textColor, $color)
    {
        $canvas = InterventionImage::canvas($image['width'], $image['height'], $background);

        $text = $image['width'] . ' X ' . $image['height'];

        if (!empty($image['text'])) {
            $text = $image['text'];
        }

        $canvas->text($text,
            $image['width'] / 2,
            $image['height'] / 2,
            function ($font) use ($textColor, $image) {
                $font->size(($image['width'] + $image['height']) / 30);
                $font->color($textColor);
                $font->file(public_path() . '/assets/fonts/arial.ttf');
                $font->valign('top');
                $font->align('center');
            });

        return array($image, $canvas->response("png"));
    }

    /**
     * Get user defined image size.
     *
     * @param $width
     * @param $height
     * @param $background
     * @param $textColor
     * @param $text
     *
     * @return array
     */
    public static function getUserDefinedImageSize($width, $height, $background, $textColor, $text)
    {
        $image = [
            'width'         => $width,
            'height'        => $height,
            'background'    => $background,
            'textColor'     => $textColor,
            'text'          => $text,
            'mime'          => 'png',
        ];

        return $image;
    }

    /**
     * Get placeholder color.
     *
     * @param $image
     * @param $color
     *
     * @return array
     */
    public static function getPlaceholderColor($image, $color)
    {
        $background = is_null($image['background']) ? $color['background'] : "{$image['background']}";
        $textColor = is_null($image['textColor']) ? $color['text'] : "{$image['textColor']}";

        return array($background, $textColor);
    }

    /**
     * Validate color HEX code.
     *
     * @param $background
     * @param $textColor
     *
     * @throws RuntimeException
     */
    public static function validateColorHexCode($background, $textColor)
    {
        if(!is_null($background)) {
            if ((strlen($background) !== 3) and (strlen($background) !== 6)) {
                throw new RuntimeException("Wrong HEX code [ #{$background} ] for your background. ");
            }
        }

        if(!is_null($textColor)) {
            if ((strlen($textColor) !== 3) and (strlen($textColor) !== 6)) {
                throw new RuntimeException("Wrong HEX code [ #{$textColor} ] for your text.");
            }
        }
    }

    /**
     * Validate image size.
     *
     * @param $width
     * @param $height
     */
    protected static function validateImageSize($width, $height)
    {
        if (!is_numeric($width)) {
            throw new RuntimeException("Width size must be integer");
        }

        if (!is_numeric($height)) {
            throw new RuntimeException("Height size must be integer");
        }
    }

    /**
     * Get uri for google analytics.
     */
    public function getUri()
    {
        $uri = $this->urlGenerator->getUri();

        if(!empty(request()->input('src'))) {
            $uri .= "?" . request()->input('src');
        }
        if(!empty(request()->input('text'))) {
            $uri .= "?" . request()->input('text');
        }

        return $uri;
    }

    /**
     * Send http request to the google analytics.
     */
    public function sendGoogleAnalytics()
    {
        $gs = new GoogleAnalytics( $this->googleAnalyticsID, 'http://faker.im');

        # Set a pageview
        $gs->set_page( $this->getUri() );
        $gs->set_page_title( 'URI: ' . $this->getUri() );

        # send
        $gs->send();
        $gs->reset();
    }
}
