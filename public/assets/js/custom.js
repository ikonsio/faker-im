
/**
* File Upload
*/
$(document).on('change', '.btn-file :file', function() {
    var input = $(this),
        numFiles = input.get(0).files ? input.get(0).files.length : 1,
        label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
        input.trigger('fileselect', [numFiles, label]);

        document.getElementById("xml-submit-form").submit();
});

$(document).ready( function() {
    //File Upload
    $('.btn-file :file').on('fileselect', function(event, numFiles, label) {

        var input = $(this).parents('.input-group').find(':text'),
            log = numFiles > 1 ? numFiles + ' files selected' : label;

        if( input.length ) {
            input.val(log);
        } else {
            if( log ) alert(log);
        }
    });

});

$('*[data-menu="about"]').click(function(){
    $(".about").addClass("slide-down");
});

$(".closebtn").click(function(){
    $(".about").removeClass("slide-down");
});
